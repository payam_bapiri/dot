#+TITLE: My emacs configuration file
#+AUTHOR: Payam Bapiri <payambapiri.97@gmail.com>
* ~
#+BEGIN_SRC emacs-lisp
  ;;;###autoload
  (defun mapcar* (function &rest args)
    (if (not (memq nil args))
        (cons (apply function (mapcar 'car args))
              (apply 'mapcar* function
                     (mapcar 'cdr args)))))
  ;;;###autoload
  (defun replace-string* (from-string to-string)
    (replace-string from-string to-string nil 0 (buffer-size)))
  ;;;###autoload
  (defun escape-string (string)
    "Backslash escaping escape-worth characters."
    (let* ((escape-worth '(" " "(" ")" "'" "&"))
           (to (mapcar #'(lambda (o) (format "\\%s" o))
                       escape-worth)))
      (with-temp-buffer
        (insert string)
        (mapcar* 'replace-string* escape-worth to)
        (buffer-string))))

  ;; `string-trim' function
  (require 'subr-x)
#+END_SRC
* /package/
#+BEGIN_SRC emacs-lisp
  (require 'package)
  ;; On bad signature error un-comment the line below.
  ;; (setq package-check-signature nil)
  (setq package-enable-at-startup nil
        package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                           ("melpa" . "http://melpa.org/packages/")))
  (package-initialize)
#+END_SRC
* /package/ use-package
#+BEGIN_SRC emacs-lisp
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))
#+END_SRC
* /package/ exwm
#+BEGIN_SRC emacs-lisp
  (when (eq 'x (window-system))
    (use-package exwm
      :ensure t
      :config
      (require 'exwm)
      (exwm-input-set-simulation-keys
       '(
         ([?\C-b] . left)
         ([?\M-b] . C-left)
         ([?\C-f] . right)
         ([?\M-f] . C-right)
         ([?\C-p] . up)
         ([?\C-n] . down)
         ([?\C-a] . home)
         ([?\C-e] . end)
         ([?\M-v] . prior)
         ([?\C-v] . next)
         ([?\C-d] . delete)
         ([?\C-k] . (S-end delete))
         ([?\C-w] . ?\C-x)
         ([?\M-w] . ?\C-c)
         ([?\C-y] . ?\C-v)
         ([?\C-s] . ?\C-f)))
      (dolist (k '(XF86AudioLowerVolume
                   XF86AudioRaiseVolume
                   XF86AudioMute
                   XF86KbdBrightnessUp
                   XF86KbdBrightnessDown
                   XF86MonBrightnessUp
                   XF86MonBrightnessDown))
        (cl-pushnew k exwm-input-prefix-keys))
      (setq exwm-input-global-keys
            `(([?\s-!] . keyboard-english)
              ([?\s-@] . keyboard-kurdish)
              ([?\s-#] . keyboard-latin-kurdish)))
      (add-hook 'exwm-update-class-hook
                (lambda ()
                  (exwm-workspace-rename-buffer
                   (format "#%s#" exwm-class-name))))
      (exwm-enable)))
#+END_SRC
* /package/ async
#+BEGIN_SRC emacs-lisp
  (use-package async
    :ensure t
    :config
    (require 'async)
    (dired-async-mode))
#+END_SRC
* /package/ bongo
#+BEGIN_SRC emacs-lisp
  (use-package bongo
    :ensure t
    :config
    (require 'bongo)
    (setq bongo-logo nil
          bongo-default-directory "~/music/"
          bongo-enabled-backends '(mplayer)))
#+END_SRC
* /package/ markdown-mode
#+BEGIN_SRC emacs-lisp
  (use-package markdown-mode :ensure t)
#+END_SRC
* /package/ web-mode
#+BEGIN_SRC emacs-lisp
  (use-package web-mode
    :ensure t
    :config
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode)))
#+END_SRC
* /package/ slime
#+BEGIN_SRC emacs-lisp
  (use-package slime
    :ensure t
    :config
    ;; Set your lisp system and, optionally, some contribs
    (setq inferior-lisp-program "/usr/bin/sbcl --noinform")
    (setq slime-contribs '(slime-fancy)))
#+END_SRC
* /package/ htmlize
#+BEGIN_SRC emacs-lisp
  ;; For org-mode HTML export
  (use-package htmlize :ensure t)
#+END_SRC
* C-mode
#+BEGIN_SRC emacs-lisp
  ;; Hook
  (add-hook 'c-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c C-c") 'c-compile-run-current-file)
              (local-set-key (kbd "C-c C-r")
                             (lambda () (interactive)
                               (c-compile-run-current-file t)))))

  ;; Function
  ;;;###autoload
  (defun buffer-exists-p (buffer-name)
    (defun buffer-exists-rec (buffer-name buffer-list)
      (if (consp buffer-list)
          (if (string= buffer-name (buffer-name (car buffer-list)))
              buffer-name
            (buffer-exists-rec buffer-name (cdr buffer-list)))))
    (buffer-exists-rec buffer-name (buffer-list)))
  ;;;###autoload
  (defun c-compile-run-current-file (&optional run)
    (interactive)
    (save-buffer)
    (let* ((in (buffer-file-name))
           (out (substring in 0 -2))
           (compiled? (c-compile-file in out "" "*c-compilation*")))
      (if (and compiled? run) (c-run-file out "" "*c-run*"))))
  ;;;###autoload
  (defun c-compile-file (in &optional out opts buffer)
    (let* ((out (or out (substring in 0 -2)))
           (command (format "cc %s -o '%s' '%s'" opts out in))
           (result (shell-command-to-string command)))
      (if (string= "" result)
          (progn (message "Compilation finished.") t)
        (if buffer
            (progn
              (unless (buffer-exists-p buffer)
                (generate-new-buffer buffer))
              (with-current-buffer buffer
                (read-only-mode -1)
                (erase-buffer) (insert result)
                (compilation-mode))
              (display-buffer buffer))))))
  ;;;###autoload
  (defun c-run-file (o &optional opts buffer)
    (let ((command (format "'%s' %s" o opts)))
      (shell-command command buffer)
      (message "") (display-buffer buffer)))
#+END_SRC
* PHP-repl
#+BEGIN_SRC emacs-lisp
  ;; Hook
  (add-hook 'web-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c C-c") 'php-IA)
              (local-set-key (kbd "C-c C-r") 'php-IA-rtl)))

  ;; Function
  ;;;###autoload
  (defun php-IA (&optional rtl)
    (interactive)
    (let* ((f (buffer-file-name))
           (tr (term "/bin/bash"))
           (rq (format "require('%s');\n" f)))

      (setq bidi-display-reordering rtl)
      (term-send-string tr "php -a\n")
      (term-send-string tr rq)))
  ;;;###autoload
  (defun php-IA-rtl () (interactive) (php-IA t))
#+END_SRC
* Quail
#+BEGIN_SRC emacs-lisp
  (add-to-list 'load-path
               (expand-file-name "langs" user-emacs-directory))
  ;; Input-methods
  ;;;###autoload
  (require 'kurdish-sorani)
  ;;;###autoload
  (require 'kurdish-kurmanci)

  ;; Key-bindings
  (global-set-key (kbd "s-1")
                  (lambda () (interactive)
                    (change-input-method nil "English")))
  (global-set-key (kbd "s-2")
                  (lambda () (interactive)
                    (change-input-method 'kurdish-sorani "کوردی")))
  (global-set-key (kbd "s-3")
                  (lambda () (interactive)
                    (change-input-method 'kurdish-kurmanci "Kurdî")))

  ;; Function
  ;;;###autoload
  (defun change-input-method (method &optional message)
    (set-input-method method)
    (message message))
#+END_SRC
* Storage
#+BEGIN_SRC emacs-lisp
  (defun memory-free ()
    (format "%.1fG"
            (/ (nth 1 (memory-info)) 1000000.0)))
  ;;;###autoload
  (defun memory-drop-caches ()
    (interactive)
    (shell-command "sudo su -c 'echo 1 > /proc/sys/vm/drop_caches'")
    (setq memory-free (memory-free))
    (mode-line-refresh)
    (message "Memory cleared. (%s)" memory-free))
#+END_SRC
* Internet
#+BEGIN_SRC emacs-lisp
  (defun local-ip-address ()
    "Private IP Address"
    (string-trim (shell-command-to-string "hostname -i")))

  (defun internet? ()
    "Check Internet Connection"
    (let ((connection (car (last (split-string
                                  (string-trim
                                   (shell-command-to-string
                                    "nmcli connect|head -2|tail -1")))))))
      (if (not (string= "--" connection))
          (local-ip-address)
        connection)))
#+END_SRC
* Appearance
#+BEGIN_SRC emacs-lisp
  ;;; Remove bars
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (fringe-mode '(0 . 0))

  ;;; Theme
  (global-set-key [XF86LaunchA] 'theme-toggle)

  (add-to-list 'load-path (expand-file-name "themes" user-emacs-directory))

  (setq custom-theme-directory
        (expand-file-name "themes" user-emacs-directory))
  (add-to-list 'custom-safe-themes 'allekok-light)
  (add-to-list 'custom-safe-themes 'allekok-dark)

  (defun theme-load* (theme)
    "Disable all enabled themes and load `theme'."
    (mapc 'disable-theme custom-enabled-themes)
    (load-theme theme t))
  ;;;###autoload
  (defun theme-toggle ()
    (interactive)
    (theme-load* (if (memq 'allekok-light
                           custom-enabled-themes)
                     'allekok-dark 'allekok-light))
    (cancel-timer theme-timer))

  (defun theme-now ()
    (let ((h (string-to-number
              (format-time-string "%H"))))
      (theme-load*
       (if (and (> h 6) (<= h 23))
           'allekok-light 'allekok-dark))))

  (setq theme-timer
        (run-with-timer 0 1800
                        #'(lambda ()
                            (theme-now))))

  ;;; Mode-line
  (defun mode-line-refresh ()
    (interactive)
    (let ((| " | "))
      (setq-default
       mode-line-format
       (list
        " " battery | datetime |
        ;; Buffer name
        '(:eval (propertize "%b" 'face
                            (when (buffer-modified-p)
                              'font-lock-warning-face)))
        | "%m" | "%l,%02c" | "%p-%I" |
        internet? | (when (volume-mute?) "MUTE ")
        (volume-level) | memory-free))))

  (defun mode-line-refresh-variables ()
    (setq datetime (format-time-string "%H:%M %a-%d-%b")
          battery battery-mode-line-string
          internet? (internet?)
          memory-free (memory-free)))

  (setq mode-line-refresh-variables-timer
        (run-with-timer 0 30
                        #'(lambda ()
                            (mode-line-refresh-variables)
                            (mode-line-refresh))))
#+END_SRC
* Time
#+BEGIN_SRC emacs-lisp
  (setq display-time-24hr-format t)
#+END_SRC
* Battery
#+BEGIN_SRC emacs-lisp
  (setq battery-mode-line-format "%p")
  (display-battery-mode 1)
#+END_SRC
* Volume
#+BEGIN_SRC emacs-lisp
  ;; Key-bindings
  (global-set-key [XF86AudioMute] 'volume-mute)
  (global-set-key [XF86AudioRaiseVolume] 'volume-raise)
  (global-set-key [XF86AudioLowerVolume] 'volume-lower)

  ;; Functions
  ;;;###autoload
  (defun volume-mute ()
    (interactive)
    (shell-command-to-string
     "amixer set Master toggle")
    (message (if (volume-mute?) "MUTE" "UNMUTE"))
    (mode-line-refresh) (redisplay))
  ;;;###autoload
  (defun volume-set (v &optional message-format)
    (let ((message-format (or message-format "* volume: %s"))
          (command (concat "pactl set-sink-volume 0 " v)))
      (start-process-shell-command command nil command)
      (mode-line-refresh) (redisplay)
      (message message-format (volume-level))))
  ;;;###autoload
  (defun volume-raise (&optional step)
    (interactive)
    (let ((step (or step "+2%")))
      (volume-set step "+ volume: %s")))
  ;;;###autoload
  (defun volume-lower (&optional step)
    (interactive)
    (let ((step (or step "-2%")))
      (volume-set step "- volume: %s")))
  ;;;###autoload
  (defun volume-level ()
    (let ((vl (string-trim
               (shell-command-to-string
                "awk -F '[][]' '{print $2}' <(amixer get Master | tail -1)"))))
      (unless (string= vl "amixer: Unable to find simple control 'Master',0")
        vl)))
  ;;;###autoload
  (defun volume-mute? ()
    (when (string= (string-trim
                    (shell-command-to-string
                     "awk -F '[][]' '{print $4}' <(amixer get Master | tail -1)"))
                   "off")
      t))
#+END_SRC
* Screen brightness
#+BEGIN_SRC emacs-lisp
  ;; Key-bindings
  (global-set-key [XF86MonBrightnessUp] 'screen-brighter)
  (global-set-key [XF86MonBrightnessDown] 'screen-darker)

  ;; Functions
  (setq screen-brightness-file
        "/sudo::/sys/class/backlight/acpi_video0/brightness")
  (setq screen-brightness-max-file
        "/sudo::/sys/class/backlight/acpi_video0/max_brightness")

  ;;;###autoload
  (defun screen-brightness-max ()
    (interactive)
    (with-temp-buffer
      (insert-file-contents screen-brightness-max-file)
      (string-to-number (buffer-string))))
  ;;;###autoload
  (defun screen-brightness-current ()
    (interactive)
    (with-temp-buffer
      (insert-file-contents screen-brightness-file)
      (string-to-number (buffer-string))))
  ;;;###autoload
  (defun screen-brightness-set (v &optional message-format)
    (interactive "nbrightness: ")
    (let ((message-format (or message-format "* brightness: %d")))
      (when (and (<= v (screen-brightness-max)) (>= v 0))
        (with-temp-file screen-brightness-file
          (insert (number-to-string v)))
        (message message-format v))))
  ;;;###autoload
  (defun screen-brighter (&optional step)
    (interactive)
    (unless step (setq step +1))
    (let ((v (+ (screen-brightness-current) step)))
      (screen-brightness-set v "+ brightness: +%d")))
  ;;;###autoload
  (defun screen-darker (&optional step)
    (interactive)
    (unless step (setq step -1))
    (let ((v (+ (screen-brightness-current) step)))
      (screen-brightness-set v "- brightness: -%d")))
#+END_SRC
* Keyboard backlight
#+BEGIN_SRC emacs-lisp
  ;; Key-bindings
  (global-set-key [XF86KbdBrightnessUp] 'kbd-brighter)
  (global-set-key [XF86KbdBrightnessDown] 'kbd-darker)

  ;; Functions
  (setq kbd-brightness-file
        "/sudo::/sys/class/leds/smc::kbd_backlight/brightness")
  (setq kbd-brightness-max-file
        "/sudo::/sys/class/leds/smc::kbd_backlight/max_brightness")
  ;;;###autoload
  (defun kbd-brightness-max ()
    (with-temp-buffer
      (insert-file-contents kbd-brightness-max-file)
      (string-to-number (buffer-string))))
  ;;;###autoload
  (defun kbd-brightness-current ()
    (with-temp-buffer
      (insert-file-contents kbd-brightness-file)
      (string-to-number (buffer-string))))
  ;;;###autoload
  (defun kbd-brightness-set (v &optional message-format)
    (interactive "nkbd backlight: ")
    (let ((message-format (or message-format "* kbd backlight: %d")))
      (when (and (<= v (kbd-brightness-max)) (>= v 0))
        (with-temp-file kbd-brightness-file
          (insert (number-to-string v)))
        (message message-format v))))
  ;;;###autoload
  (defun kbd-brighter (&optional step)
    (interactive)
    (unless step (setq step +1))
    (let ((v (+ (kbd-brightness-current) step)))
      (kbd-brightness-set v "+ kbd backlight: +%d")))
  ;;;###autoload
  (defun kbd-darker (&optional step)
    (interactive)
    (unless step (setq step -1))
    (let ((v (+ (kbd-brightness-current) step)))
      (kbd-brightness-set v "- kbd backlight: -%d")))
#+END_SRC
* Initial buffer
#+BEGIN_SRC emacs-lisp
  (setq inhibit-startup-screen t
        initial-scratch-message "")
  (defun display-startup-echo-area-message ()
    (message "Hi"))
#+END_SRC
* Text-mode
#+BEGIN_SRC emacs-lisp
  (setq-default major-mode 'text-mode)
  (add-hook 'text-mode-hook 'auto-fill-mode)
#+END_SRC
* Org
#+BEGIN_SRC emacs-lisp
  (require 'org)
  (setq org-export-coding-system 'utf-8
        org-src-window-setup 'current-window
        org-directory "~/projects/org"
        org-default-notes-file "~/projects/org/notes.org"
        org-hide-leading-stars t
        org-startup-indented t
        org-confirm-babel-evaluate nil)
  (add-to-list 'org-file-apps '(directory . emacs))
  (add-to-list 'org-file-apps '("\\.pdf\\'" . "mupdf %s"))
  (add-hook 'org-mode-hook 'org-display-inline-images)

  (global-set-key (kbd "C-c c") 'org-capture)
  (global-set-key (kbd "C-c a") 'org-agenda)
  (setq org-agenda-start-on-weekday nil
        org-agenda-files '("~/projects/plan.org"
                           "~/projects/org/notes.org"))
#+END_SRC
* Keyboard languages
#+BEGIN_SRC emacs-lisp
  ;;;###autoload
  (defun keyboard-language (layout &optional variant message)
    (start-process-shell-command
     "keyboard-language" nil
     (format "setxkbmap -layout %s -variant %s"
             layout variant))
    (message message))
  ;;;###autoload
  (defun keyboard-english () (interactive)
         (keyboard-language "us" "" "English"))
  ;;;###autoload
  (defun keyboard-kurdish () (interactive)
         (keyboard-language "ir" "ku_ara" "کوردی"))
  ;;;###autoload
  (defun keyboard-latin-kurdish () (interactive)
         (keyboard-language "ir" "ku" "Kurdî"))
#+END_SRC
* Kurdish font-face
#+BEGIN_SRC emacs-lisp
  (let ((spec (font-spec :family "NotoNaskhArabicUI" :weight 'bold)))
    (set-fontset-font nil 'arabic spec)
    (set-fontset-font nil #x200c spec))
#+END_SRC
* Desktop apps
#+BEGIN_SRC emacs-lisp
  ;; Functions
  ;;;###autoload
  (defun desktop-app-open (app &optional args escape)
    (when (and escape args)
      (setq args (escape-string args)))
    (start-process-shell-command
     app nil (concat app " " args)))
  ;;;###autoload
  (defmacro desktop-app (app &optional escape prompt)
    (let* ((app-str (symbol-name app))
           (prompt (and prompt (format "%s%s: " prompt app-str))))
      `(defun ,app (&optional args)
         (interactive ,prompt)
         (desktop-app-open ,app-str args ,escape))))

  ;; Apps
  ;;;###autoload
  (desktop-app firefox)
  ;;;###autoload
  (desktop-app chromium)
  ;;;###autoload
  (desktop-app surf t "s")
  ;;;###autoload
  (desktop-app st)
  ;;;###autoload
  (desktop-app mupdf t "f")
  ;;;###autoload
  (desktop-app vlc t "f")
  ;;;###autoload
  (desktop-app mpv t "f")
  ;;;###autoload
  (desktop-app gimp t "f")
  ;;;###autoload
  (defun tor-browser (&optional args)
    (interactive)
    (shell-command
     "cd ~/projects/tor-browser_en-US/ && ./start-tor-browser.desktop"))
  ;;;###autoload
  (defun tchromium (&optional args)
    (interactive)
    (chromium (concat "--proxy-server=socks://127.0.0.1:9150 " args)))
#+END_SRC
* Coding system <- UTF-8
#+BEGIN_SRC emacs-lisp
  (set-language-environment "UTF-8")
  (set-default-coding-systems 'utf-8)
  (setq-default locale-coding-system 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (prefer-coding-system 'utf-8)
#+END_SRC
* Kill-buffer
#+BEGIN_SRC emacs-lisp
  ;; Key-bindings
  (global-set-key (kbd "C-x k") 'kill-this-buffer)
  (global-set-key (kbd "C-x C-k") 'kill-this-buffer)
  ;; Kill all buffers
  (global-set-key (kbd "C-x C-z") 'kill-buffers-all)
  ;; Unset key-binding
  (global-unset-key (kbd "C-z"))

  ;; Functions
  ;;;###autoload
  (defun kill-buffers-all () (interactive)  
         (mapc 'kill-buffer (buffer-list))
         (cd "~/")
         (message "All buffers killed."))
#+END_SRC
* Find-file
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-x f") 'find-file)
#+END_SRC
* Dired
#+BEGIN_SRC emacs-lisp
  ;; Hooks
  (setq dired-listing-switches "-alh --group-directories-first")
  (global-set-key (kbd "C-x C-d") 'dired)
  (add-hook 'dired-mode-hook 'dired-hide-details-mode)
  (add-hook 'dired-mode-hook
            #'(lambda ()
                (local-set-key
                 (kbd "!") #'(lambda (program)
                               (interactive
                                (list (read-shell-command "Program: ")))
                               (my-dired-shell-command program)))
                (local-set-key
                 (kbd "@") 'my-dired-run-http-server)
                (local-set-key
                 (kbd "<return>") 'my-dired-uni-open)))

  ;; Functions
  ;;;###autoload
  (defun my-dired-uni-open ()
    (interactive)
    (let ((file (dired-get-file-for-visit)))
      (cond
       ((file-directory-p file) (dired-find-file))
       ((string-suffix-p ".avi" file t) (mpv file))
       ((string-suffix-p ".mp4" file t) (mpv file))
       ((string-suffix-p ".mp3" file t) (vlc file))
       ((string-suffix-p ".wav" file t) (vlc file))
       ((string-suffix-p ".m4v" file t) (mpv file))
       ((string-suffix-p ".m4a" file t) (vlc file))
       ((string-suffix-p ".mkv" file t) (mpv file))
       ((string-suffix-p ".pdf" file t) (mupdf file))
       ((string-suffix-p ".xcf" file t) (gimp file))
       (t (dired-find-file)))))
  ;;;###autoload
  (defun my-dired-shell-command (program)
    (let ((file (dired-get-file-for-visit)))
      (start-process-shell-command
       "my-dired-shell-command" nil
       (concat program " " (escape-string file)))))
  ;;;###autoload
  (defun my-dired-run-http-server ()
    (interactive)
    (let ((file (dired-get-file-for-visit)))
      (if (file-directory-p file)
          (st (concat "php -S localhost:8081 -t "
                      (escape-string file)
                      " & chromium --app=http://localhost:8081")))))
#+END_SRC
* Backup and autosaving
#+BEGIN_SRC emacs-lisp
  (setq make-backup-files nil
        auto-save-interval 100)
#+END_SRC
* Scrolling
#+BEGIN_SRC emacs-lisp
  (setq scroll-step 1
        scroll-conservatively 5)
#+END_SRC
* Tramp
#+BEGIN_SRC emacs-lisp
  (setq tramp-default-method "ssh"
        tramp-verbose -1)
#+END_SRC
* yes-or-no <- y-or-n
#+BEGIN_SRC emacs-lisp
  (fset 'yes-or-no-p 'y-or-n-p)
#+END_SRC
* C-x-(a A !)
#+BEGIN_SRC emacs-lisp
  ;;; allekok.com
  ;; Open website
  (global-set-key (kbd "C-x a")
                  #'(lambda () (interactive)
                      (chromium "--app=http://localhost/")))
  ;; Test server
  (global-set-key (kbd "C-x A")
                  #'(lambda () (interactive)
                      (chromium "http://localhost/")))
  ;; Show allekok/status
  (global-set-key (kbd "C-x !")
                  #'(lambda () (interactive)
                      (switch-to-buffer "allekok/status")
                      (erase-buffer)
                      (url-insert-file-contents
                       "http://localhost/status.php")
                      (message "'allekok/status' Done!")
                      (org-mode)
                      (setq bidi-paragraph-direction 'right-to-left)))
#+END_SRC
* Hippie-expand
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "s-<tab>") 'hippie-expand)
#+END_SRC
* Switch-buffer
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-x C-b") 'switch-to-buffer)
#+END_SRC
* Paren-mode
#+BEGIN_SRC emacs-lisp
  (setq show-paren-delay .1)
  (show-paren-mode)
#+END_SRC
* Other-window
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-x C-o") 'other-window)
#+END_SRC
* Hideshow-mode
#+BEGIN_SRC emacs-lisp
  ;; Hooks
  (add-hook 'prog-mode-hook 'hs-minor-mode)
  (add-hook 'hs-minor-mode-hook
            #'(lambda ()
                (local-set-key (kbd "s-~") 'hs-toggle-all)))

  ;; Functions
  (setq hs-status-all 'show)
  ;;;###autoload
  (defun hs-toggle-all ()
    (interactive)
    (if (eq 'show hs-status-all)
        (progn (hs-hide-all)
               (setq hs-status-all 'hide))
      (progn (hs-show-all)
             (setq hs-status-all 'show))))
#+END_SRC
* Zoom
#+BEGIN_SRC emacs-lisp
  (define-key ctl-x-map [?+] 'text-scale-adjust)
  (define-key ctl-x-map [?=] 'text-scale-adjust)
  (define-key ctl-x-map [?-] 'text-scale-adjust)
#+END_SRC
* Bidi-direction
#+BEGIN_SRC emacs-lisp
  ;; Key-bindings
  (global-set-key [XF86LaunchB] 'bidi-toggle)

  ;; Functions
  ;;;###autoload
  (defun bidi-toggle ()
    (interactive)
    (setq bidi-paragraph-direction
          (if (eq bidi-paragraph-direction
                  'right-to-left)
              'left-to-right 'right-to-left)))
#+END_SRC
* Git
#+BEGIN_SRC emacs-lisp
  ;; Key bindings
  (global-set-key (kbd "s-`")
                  (lambda () (interactive)
                    (git-dir default-directory "status" t)))

  ;; Functions
  ;;;###autoload
  (defun git-dir (dir command &optional rtl)
    (interactive)
    (let ((o (term "/bin/bash")))
      (term-send-string o (format "git %s\n" command))
      (setq bidi-display-reordering rtl)))
#+END_SRC
* Electric
#+BEGIN_SRC emacs-lisp
  (electric-indent-mode 1)
  (electric-pair-mode 1)
#+END_SRC
* Misc
#+BEGIN_SRC emacs-lisp
  (blink-cursor-mode -1)
  (setq-default fill-column 80
                line-spacing 7)
  (auto-image-file-mode)
  (global-set-key (kbd "C-x e") 'eval-last-sexp)
  (global-set-key (kbd "C-<return>") 'calculator)
  (when (boundp 'image-map)
    (define-key image-map "=" 'image-increase-size))
  (setq safe-local-variable-values
        '((bidi-paragraph-direction . right-to-left))
        shr-use-colors nil)
#+END_SRC
* Compile
#+BEGIN_SRC emacs-lisp
  (unless (fboundp 'file-attribute-modification-time)
    ;;; From 'files.el'
    (defsubst file-attribute-modification-time (attributes)
      "The modification time in ATTRIBUTES returned by `file-attributes'.
  This is the time of the last change to the file's contents, and
  is a list of integers (HIGH LOW USEC PSEC) in the same style
  as (current-time)."
      (nth 5 attributes)))
  ;;;###autoload
  (defun modif-time (f)
    (let ((m (file-attribute-modification-time
              (file-attributes f))))
      (and m (+ (nth 0 m)
                (/ (nth 1 m) (expt 2.0 16))))))
  ;;;###autoload
  (defun modif-time-more-recent (f1 f2)
    (let ((m1 (modif-time f1))
          (m2 (modif-time f2)))
      (or (not m2) (> m1 m2))))
  ;;;###autoload
  (defun compile-if-necessary (f)
    (let* ((org? (string-suffix-p ".org" f t))
           (el? (string-suffix-p ".el" f t))
           (o (concat (substring f 0 (if org? -3 -2)) "elc"))
           (compile? (modif-time-more-recent f o)))
      (if compile?
          (progn (setq byte-compile-warnings nil)
                 (if org?
                     (byte-compile-file
                      (car (org-babel-tangle-file
                            f (concat (substring f 0 -3) "el"))))
                   (byte-compile-file f))))))
  ;;;###autoload
  (defun my-compile-all ()
    (interactive)
    (mapcar 'compile-if-necessary
            (list
             (expand-file-name
              "init.el" user-emacs-directory)
             (expand-file-name
              "config.org" user-emacs-directory)
             (expand-file-name
              "themes/allekok-core-theme.el" user-emacs-directory)
             (expand-file-name
              "themes/allekok-dark-theme.el" user-emacs-directory)
             (expand-file-name
              "themes/allekok-light-theme.el" user-emacs-directory)
             (expand-file-name
              "langs/kurdish-sorani.el" user-emacs-directory)
             (expand-file-name
              "langs/kurdish-kurmanci.el" user-emacs-directory))))

  (global-set-key [XF86AudioPlay] 'my-compile-all)
  (add-hook 'kill-emacs-hook 'my-compile-all)
#+END_SRC
* Server
#+BEGIN_SRC emacs-lisp
  (server-start)
#+END_SRC
