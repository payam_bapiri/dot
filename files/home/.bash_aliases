# -*- mode:sh -*-
alias ls='ls --color=auto'
alias vlcrec='vlc --recursive expand'
alias em='startx 2> /dev/null'
alias feh='feh -B "white"'
